# PHP Folder

Put here the ini files to configure PHP parameters.

Here is how to include a ini file in [docker-compose.yml](../docker-compose.yml) :

```yaml
wordpress:
    volumes:
      - ./php/90-custom.ini:/usr/local/etc/php/conf.d/90-custom.ini
```

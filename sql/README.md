# SQL Folders

## conf
List of mysql configuration files to optimize mysql server behavior.

Here is the volume in [docker-compose.yml](../docker-compose.yml) file that enables this behavior :

```yaml
db:
    volumes:
      - ./sql/conf:/etc/mysql/conf.d
```

## dump
Destination directory for database dumps done with *make dbdump* command.

```bash
make dbdump
```

## initdb
Sql dumps in this directory will be imported in db container only *if database is empty*.

Here is the volume in [docker-compose.yml](../docker-compose.yml) file that enables this behavior :

```yaml
db:
    volumes:
      - ./sql/initdb:/docker-entrypoint-initdb.d/
```

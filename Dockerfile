FROM registry.gitlab.com/hostinauvergne/wordpress:5.0.3-php7.3

COPY --chown=www-data:www-data www/.htaccess /var/www/html/
COPY --chown=www-data:www-data  www/languages /var/www/html/wp-content/languages/
COPY --chown=www-data:www-data  www/plugins /var/www/html/wp-content/plugins/
COPY --chown=www-data:www-data  www/themes /var/www/html/wp-content/themes/
COPY php/90-custom.ini /usr/local/etc/php/conf.d/
